package com.finnbear.circles;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import javax.imageio.ImageIO;
import javax.swing.*;

public class Main extends JFrame {
    public static void main(String[] args) {
        Runnable starter = new Runnable() {
            @Override
            public void run() {
                new Main();
            }
        };

        SwingUtilities.invokeLater(starter);
    }

    private static BufferedImage context;
    private static JPanel contextPanel;
    private static JLabel contextRender;
    private static RenderingHints antialiasing;

    public Main() {
        super("Circle Packing (Finn Bear)");

        int width = 800;
        int height = 800;
        int padding = 6;

        contextPanel = new JPanel();

        antialiasing = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        context =  new BufferedImage(width + (2 * padding), height + (2 * padding), BufferedImage.TYPE_INT_RGB);
        contextRender = new JLabel(new ImageIcon(context));

        PanelListener mouseListener = new PanelListener();

        contextPanel.add(contextRender);
        contextPanel.setSize(width + padding * 2, height + padding * 2);
        contextPanel.addMouseListener(mouseListener);
        JFrame.setDefaultLookAndFeelDecorated(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setContentPane(contextPanel);
        this.pack();
        this.setLocationRelativeTo(null);

        // How many circles (6, 9, 28, 50)
        this.paint(50);

        this.setVisible(true);
    }

    private class PanelListener implements MouseListener {
        int circleCount = 1;

        @Override
        public void mouseClicked(MouseEvent event) {
            Object source = event.getSource();
            if (source instanceof JPanel) {
                JPanel panelPressed = (JPanel) source;
                System.out.println(++circleCount + ", " + Main.paint(circleCount));
            }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {}

        @Override
        public void mouseExited(MouseEvent arg0) {}

        @Override
        public void mousePressed(MouseEvent arg0) {}

        @Override
        public void mouseReleased(MouseEvent arg0) {}
    }


    public static double paint(int circleCount) {
        double circleRadius = 10 + 200.0 / circleCount;
        int pawnCount = (int)Math.max(circleCount, 50);
        int pawnRadius = 1000 + (int)(circleRadius * circleCount);

        List<Point2D> circles = new ArrayList<>();

        int x = 0;
        int y = 0;

        int direction = 0;

        int side = 1;
        int nextDirection = 0;

        for (int i = 0; circles.size() < circleCount; i++) {
            if (nextDirection == side) {
                direction += 1;

                if (direction == 1) {
                    nextDirection = 1;
                } else {
                    nextDirection = 0;
                }

                if (direction == 6) {
                    direction = 0;
                    side += 1;
                }
            }

            if (nextDirection != side) {
                circles.add(new Point2D.Double(x, y));

                switch (direction) {
                    case 0: // Right
                        x += circleRadius * 2;
                        break;
                    case 1: // Right, Down
                        x += circleRadius;
                        y -= circleRadius * Math.sqrt(3);
                        break;
                    case 2: // Left, Down
                        x -= circleRadius;
                        y -= circleRadius * Math.sqrt(3);
                        break;
                    case 3: // Left
                        x -= circleRadius * 2;
                        break;
                    case 4: // Left, Up
                        x -= circleRadius;
                        y += circleRadius * Math.sqrt(3);
                        break;
                    case 5: // Right, Up
                        x += circleRadius;
                        y += circleRadius * Math.sqrt(3);
                        break;
                }

                nextDirection += 1;

                //System.out.println(direction + ", (" + nextDirection + "/" + side + ")");
            }
        }

        List<Point2D> pawns = new ArrayList<>();

        for (int i = 0; i < pawnCount; i++) {
            Point2D pawn = new Point2D.Double(pawnRadius * Math.cos(i * (2 * Math.PI / pawnCount)), pawnRadius * Math.sin(i * (2 * Math.PI / pawnCount)));

            boolean done = false;

            while (!done) {

                for (Point2D circle : circles) {
                    if (circle.distance(new Point2D.Double(pawn.getX() * 0.99, pawn.getY() * 0.99)) < circleRadius) {
                        done = true;
                    }
                }

                pawn.setLocation(pawn.getX() * 0.995, pawn.getY() * 0.995);
            }

            pawns.add(pawn);
        }

        Point2D center = new Point2D.Double(0, 0);

        for (Point2D circle : circles) {
            center.setLocation(center.getX() + circle.getX(), center.getY() + circle.getY());
        }

        center.setLocation(center.getX() / circles.size(), center.getY() / circles.size());

        int searchRange = (int)(6 + circleCount * 0.075);
        double searchTolerance = Math.min(0.6 + circleCount * 0.021, 0.95);

        boolean foundConcavity = true;

        while (foundConcavity) {
            boolean foundConcavityHere = false;

            for (int i = 0; i < pawns.size() && !foundConcavityHere; i++) {
                Point2D p = pawns.get(i);

                Point2D n1 = pawns.get(i > 0 ? i - 1 : pawns.size() - 1);
                Point2D n2 = pawns.get(i < pawns.size() - 1 ? i + 1 : 0);

                double d = p.distanceSq(center);
                double d1 = n1.distanceSq(center);
                double d2 = n2.distanceSq(center);

                //System.out.println(d + ", " + d1 + ", " + d2);

                if (d1 > d + 1 && d2 > d + 1) {
                    pawns.remove(p);
                    foundConcavityHere = true;
                }
            }

            foundConcavity = foundConcavityHere;
        }

        Graphics2D graphics = context.createGraphics();
        graphics.setRenderingHints(antialiasing);

        Font font = graphics.getFont();
        graphics.setFont(font);
        FontMetrics fontMetrics = graphics.getFontMetrics();

        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, context.getWidth(), context.getHeight());

        graphics.setStroke(new BasicStroke(5 ));

        /*
        Path2D line = new Path2D.Double();
        line.moveTo(x1, y1);
        line.lineTo(x2, y2);
        line.closePath();

        graphics.draw(line);
        */

        graphics.setStroke(new BasicStroke(3.0f));

        for (Point2D circle : circles) {
            Ellipse2D outline = getEllipse(context, circle, circleRadius);

            graphics.setColor(Color.RED);
            graphics.fill(outline);
            graphics.setColor(Color.BLACK);
            graphics.draw(outline);
        }

        graphics.setStroke(new BasicStroke(10.0f));
        graphics.setColor(Color.BLUE);

        int drawX = (int)(center.getX() + context.getWidth() * 0.5);
        int drawY = (int)(center.getY() + context.getHeight() * 0.5);

        graphics.drawLine(drawX, drawY, drawX, drawY);

        double distance = 0;

        for (int i = 0; i < pawns.size(); i++) {
            Point2D p = pawns.get(i);
            Point2D n = pawns.get(i < pawns.size() - 1 ? i + 1 : 0);

            distance += p.distance(n);

            int drawX1 = (int)(p.getX() + context.getWidth() * 0.5);
            int drawY1 = (int)(p.getY() + context.getHeight() * 0.5);

            int drawX2 = (int)(n.getX() + context.getWidth() * 0.5);
            int drawY2 = (int)(n.getY() + context.getHeight() * 0.5);

            graphics.drawLine(drawX1, drawY1, drawX2, drawY2);
        }

        graphics.drawString("Circles: " + circleCount, 10, 20);

        distance /= circleRadius * 2;

        double returnValue = distance;

        graphics.drawString("Perimeter: " + distance + " * d", 10, 40);

        distance *= circleRadius * 2;

        distance -= circleRadius * 2 * Math.PI;
        distance /= circleRadius * 2;

        graphics.drawString("Perimeter: " + distance + " * d + d * PI", 10, 60);

        contextRender.repaint();

        /*
        try {
            ImageIO.write(context, "png", new File("network.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        return returnValue;
    }

    private static Ellipse2D getEllipse(BufferedImage context, Point2D center, double radius)
    {
        Ellipse2D.Double ellipse = new Ellipse2D.Double(center.getX() - radius + context.getWidth() * 0.5, center.getY() - radius + context.getHeight() * 0.5, 2 * radius, 2 * radius);
        return ellipse;
    }
}
