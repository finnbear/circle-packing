# circle-packing

Program to find the perimeter of a convex shape containing a number of circles.

# Interface

![](interface-6.png)

![](interface-50.png)

# Graph
The blue crosses were not incorporated into the regression. They are there to demonstrate the predictive capability of the model.
![](graph-1.png)
![](regression-1.png)